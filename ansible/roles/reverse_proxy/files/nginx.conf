user nginx;
worker_processes auto;
worker_rlimit_nofile 30000;
worker_priority 0;
timer_resolution 100ms;

events {
    worker_connections 2048;
    accept_mutex on;
    accept_mutex_delay 200ms;
    use epoll;
}

http {
    charset utf-8;
    server_tokens off;
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    # BUFFERS
    client_body_buffer_size 256k;
    client_max_body_size 20m;
    client_header_buffer_size 8k;
    large_client_header_buffers 8 8k;

    # GZIP COMPRESSION
    gzip on;
    gzip_vary on;
    gzip_min_length 1400;
    gzip_disable "MSIE [1-6]\.";
    gzip_static on;
    gzip_buffers 32 8k;
    gzip_http_version 1.0;
    gzip_comp_level 5;
    gzip_proxied any;
    gzip_types text/plain text/css text/xml application/javascript application/x-javascript application/xml application/xml+rss application/ecmascript application/json image/svg+xml;

    # BROTLI COMPRESSION
    # brotli on;
    # brotli_comp_level 6;
    # brotli_static on;
    # brotli_types application/atom+xml application/javascript application/json application/rss+xml application/vnd.ms-fontobject application/x-font-opentype application/x-font-truetype application/x-font-ttf application/x-javascript application/xhtml+xml application/xml font/eot font/opentype font/otf font/truetype image/svg+xml image/vnd.microsoft.icon image/x-icon image/x-win-bitmap text/css text/javascript text/plain text/xml;

    access_log /dev/stdout;
    error_log /dev/stderr;

    include /etc/nginx/sites-enabled/*.conf;

    server {
        listen 80 default_server;
        root /usr/share/nginx/html;
        index index.html;

        location '/.well-known/acme-challenge' {
            default_type "text/plain";
            root /var/www/certbot;
        }

        location / {
            return 301 https://$http_host$request_uri;
        }
    }
}
