---
- name: Check if lets encrypt certificates exist for the provided stacks
  tags: certificates
  ansible.builtin.stat:
    path: /etc/letsencrypt/live/{{ item.key }}
    get_attributes: False
    get_checksum: False
    get_md5: False
    get_mime: False
  register: certificate_check
  loop: "{{ domain_list | dict2items }}"

- name: Check if nginx service is already running
  tags: certificates
  ansible.builtin.shell:
    cmd: docker service ls | grep {{ stack_name }}_nginx | wc -l
  register: proxy_check
  changed_when: False

- name: Request SSL certificates using certbot
  tags: certificates
  community.docker.docker_container:
    name: certbot_new_ssl
    image: certbot/certbot
    tty: True
    interactive: True
    detach: False
    volumes:
      - /etc/letsencrypt:/etc/letsencrypt
      - /var/lib/letsencrypt:/var/lib/letsencrypt
      - /home/{{ non_root_user }}/stacks/{{ stack_name }}:/var/www/certbot
    ports: "{{ certbot_ports }}"
    command: |
      certonly
        --agree-tos
        --non-interactive
        --preferred-challenges {{ certbot_challenge_type }}
        --keep-until-expiring
        --server {{ certbot_prod_server }}
        --debug
        {{ certbot_mode }}
        --email {{ certbot_contact_email }}
        --cert-name {{ item.item.key }}
        -w /var/www/certbot
        {{ certbot_domain_params }}
  vars:
    names: "{{ item.item.value | split(' ') }}"
    certbot_domain_params: "{{ ['-d'] | product(names) | map('join', ' ') | join(' ') }}"
    is_proxy_running: "{{ proxy_check.stdout | int > 0 }}"
    certbot_mode: "{{ is_proxy_running | ternary('--webroot', '--standalone') }}"
    certbot_ports: "{{ is_proxy_running | ternary(['127.0.0.1:8080:8080'], ['80:80', '443:443']) }}"
  register: certs
  loop: "{{ certificate_check.results }}"
  when: not item.stat.exists or certbot_renew
