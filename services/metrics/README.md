### Dynamic configuration values

The [influxdb Docker image does not accept environment variables](https://github.com/influxdata/influxdata-docker/issues/328) to override values found in the configuration. In order to avoid hard-coding sensitive values inside the configuration file you can instead [use environment variables](https://github.com/influxdata/telegraf/blob/master/docs/CONFIGURATION.md#environment-variables=).

Try:

`/etc/default/telegraf`

```
USER="alice"
INFLUX_URL="http://localhost:8086"
INFLUX_PASSWORD="monkey123"
```

`/etc/telegraf/telegraf.conf`

```toml
[global_tags]
  user = "${USER}"

# For InfluxDB 1.x:
[[outputs.influxdb]]
  urls = ["${INFLUX_URL}"]
  password = "${INFLUX_PASSWORD}"
```

---

YAML does not support command substitution or any of the features of bash scripting. It can however recognize variables in format of `${variable:default}`.

In order to deploy a service with *n* number of replicas, one per node in the swarm, you can count the number of nodes and store that in a varible. Run the docker command immediately right after:

```sh
REPLICAS=$(docker node ls | grep -v HOSTNAME | wc -l) \
docker stack deploy -c docker-compose.yml myService
```
```yml
deploy:
    replicas: ${REPLICAS:1}
```